class User{
   late int id;
   String name;
   String email;
   String password;
   String mobile;
   
   User(this.name,this.email,this.password,this.mobile);
   Map<String,dynamic> toUserMap(){
     return {
       'name':name,
       'email':email,
       'password':password,
       'mobile':mobile,

     };
   }

  static  fromMap(Map c) {

     return User(c['name'],c['email'],c['password'],c['mobile']);


  }

}