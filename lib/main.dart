import 'package:flutter/material.dart';
import 'package:flutter_sqlite/home/home.dart';
import 'package:flutter_sqlite/signup_login/LoginPage.dart';
import 'package:flutter_sqlite/signup_login/SignupPage.dart';
import 'package:flutter_sqlite/splashpage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.light().copyWith(
        colorScheme: ColorScheme.light().copyWith(
          primary: Colors.indigo,
          secondary: Colors.indigoAccent[300]
        )
      ),
      title: 'Material App',
      initialRoute: '/',
      routes: {
        '/':(_)=>SplashPage(),
        '/splash':(_)=>SplashPage(),
        '/home':(_)=>Homepage(),
        '/login':(_)=>LoginPage(),
        '/signup':(_)=>SignupPage(),
      },
    );
  }
}